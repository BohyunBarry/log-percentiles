package Partitioner;

import java.util.List;
import java.util.stream.Collectors;

import Access.AccessLog;

public class RangePartitioner {
	
	public RangePartitioner(List<Integer> list) {
		this.list = list.stream().sorted().collect(Collectors.toList());
	}
	
	private final List<Integer> list;
	
	//get number of partitions
	public int numPartitions() {
		return list.size();
	}
	
	public int getPartition(AccessLog log) {
		int index = 0;
		for (; index < list.size(); index++) {
			if (log.getResponseTime() <= list.get(index)) {
				return index;
			}
		}
		return index;
	}
}
