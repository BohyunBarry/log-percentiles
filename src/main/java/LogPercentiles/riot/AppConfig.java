package LogPercentiles.riot;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;
import com.opencsv.bean.CsvToBeanBuilder;
import com.yahoo.sketches.sampling.ReservoirItemsSketch;

import Access.AccessLog;
import Partitioner.RangePartitioner;

@Configuration
public class AppConfig {
	
	@Value("${log.dir:/var/log/httpd}")
	private String dir;
	
	@Value("${shuffle.partitions:5}")
	private int partitions;
	
	@Bean
	public RangePartitioner getPartitioner() throws IOException {
		return new RangePartitioner(getReservoirSampling());
	}
	
	public List<Integer> getReservoirSampling() throws IOException {
		ReservoirItemsSketch<Integer> sketch = ReservoirItemsSketch.newInstance(partitions);
		Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path path, BasicFileAttributes attr) throws IOException {
				try (Reader reader = new FileReader(path.toFile())) {
					List<AccessLog> logs = new CsvToBeanBuilder<AccessLog>(reader).withType(AccessLog.class).build()
							.parse();
					logs.forEach(log -> sketch.update(log.getResponseTime()));
				}
				return FileVisitResult.CONTINUE;
			}
		});
		return Lists.newArrayList(sketch.getSamples());
	}
}
